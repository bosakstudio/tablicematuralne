package pl.ct8.monteth.tablicematuralne2017.mathactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import pl.ct8.monteth.tablicematuralne2017.R;

import static android.support.design.R.id.scroll;

public class Math9Activity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math9);
        setTitle("GEOMETRIA ANALITYCZNA");
    }
}

package pl.ct8.monteth.tablicematuralne2017.phyactivities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import pl.ct8.monteth.tablicematuralne2017.R;

public class Phy2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy2);
        setTitle("KOD GENETYCZNY");
    }
}

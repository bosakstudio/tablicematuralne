package pl.ct8.monteth.tablicematuralne2017.phyactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.ct8.monteth.tablicematuralne2017.R;

public class Phy3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy3);
        setTitle("POTENCJAŁ WODY W KOMÓRCE ROŚLINNEJ I RÓWNANIE HARDY'EGO-WEINBERGA");
    }
}

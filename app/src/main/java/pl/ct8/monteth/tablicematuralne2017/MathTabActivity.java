package pl.ct8.monteth.tablicematuralne2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pl.ct8.monteth.tablicematuralne2017.mathactivities.*;

public class MathTabActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math_tab);

        findViewById(R.id.activity_math_tab_button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math1Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math2Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math3Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math4Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math5Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math6Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math7Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math8Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math9Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math10Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math11Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math12Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math13Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math14Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math15Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math16Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button17).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math17Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_math_tab_button18).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MathTabActivity.this, Math18Activity.class);
                startActivity(intent);
            }
        });

    }
}

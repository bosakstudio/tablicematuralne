package pl.ct8.monteth.tablicematuralne2017.mathactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.ct8.monteth.tablicematuralne2017.R;

public class Math18Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math18);
        setTitle("TABLICA WARTOŚCI FUNKCJI TRYGONOMETRYCZNYCH");
    }
}

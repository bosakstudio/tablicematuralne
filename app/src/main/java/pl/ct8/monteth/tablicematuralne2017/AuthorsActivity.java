package pl.ct8.monteth.tablicematuralne2017;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import static android.provider.Telephony.Mms.Part.TEXT;
import static pl.ct8.monteth.tablicematuralne2017.R.id.textView;

public class AuthorsActivity extends AppCompatActivity{

    TextView email;
    private ClipboardManager myClipboard;
    private ClipData myClip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authors);
        email = (TextView) findViewById(R.id.activity_authors_email);
        myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);

        findViewById(R.id.activity_authors_email).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {String text = email.getText().toString();
                myClip = ClipData.newPlainText("String", text);
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(getApplicationContext(), "Skopiowano!",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}

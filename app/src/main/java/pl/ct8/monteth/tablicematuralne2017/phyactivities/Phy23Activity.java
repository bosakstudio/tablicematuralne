package pl.ct8.monteth.tablicematuralne2017.phyactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.ct8.monteth.tablicematuralne2017.R;

public class Phy23Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy23);
        setTitle("FUNKCJE TRYGONOMETRYCZNE");
    }
}

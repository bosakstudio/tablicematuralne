package pl.ct8.monteth.tablicematuralne2017;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy10Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy11Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy12Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy13Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy14Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy15Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy16Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy17Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy18Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy19Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy1Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy20Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy21Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy22Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy23Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy2Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy3Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy4Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy5Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy6Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy7Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.Phy8Activity;
import pl.ct8.monteth.tablicematuralne2017.phyactivities.PhyMendeleevActivity;

public class PhyTabActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy_tab);

        findViewById(R.id.activity_phy_tab_button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy1Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy2Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy3Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy4Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy5Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy6Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy7Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy8Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy10Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button11).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy11Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button12).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy12Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button13).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy13Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button14).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy14Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy15Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button16).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy16Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button17).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy17Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button18).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy18Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button19).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy19Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button20).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy20Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button21).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy21Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button22).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy22Activity.class);
                startActivity(intent);
            }
        });
        findViewById(R.id.activity_phy_tab_button23).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PhyTabActivity.this, Phy23Activity.class);
                startActivity(intent);
            }
        });



    }
}

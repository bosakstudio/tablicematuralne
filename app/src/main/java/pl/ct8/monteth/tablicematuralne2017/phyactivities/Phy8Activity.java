package pl.ct8.monteth.tablicematuralne2017.phyactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.ct8.monteth.tablicematuralne2017.R;

public class Phy8Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phy8);
        setTitle("UKŁAD OKRESOWY PIERWIASTKÓW");
    }
}
